;;; gorgeHousse-theme.el --- Dark color theme.
;; Copyright (C) 2018 Gerry Agbobada

;; Author: Gerry Agbobada
;; URL: <Not Defined>

;; Version: 0.1.0
;; Keywords: faces
;; Package-Requires: ((emacs "24"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;; This file is not part of Emacs.

;;; Commentary:

;; Port of vim-gorgeHousse: https://framagit.org/gagbo/vim-gorgeHousse , a dark
;; and cold color theme
;; with with focus on clearly defined contrasting colors and relative ease of
;; use.

;;; Credits

;; Nasser Alshammari
;; https://github.com/nashamri/spacemacs-theme
;; I used this theme as a base for how to make an Emacs color theme

;; Kelvin Smith
;; https://github.com/oneKelvinSmith/monokai-emacs
;; Used as a reference for 256 color handling

;; Daniel Berg
;; https://github.com/srcery-colors/srcery-emacs
;; Used as a reference implementation

;;; Code:

;;;; Version checking and custom font variables
(unless (>= emacs-major-version 24)
  (error "The gorgeHousse theme requires Emacs 24 or later!"))

(deftheme gorgeHousse "gorgeHousse color theme")

(defgroup gorgeHousse nil
  "gorgeHousse options."
  :group 'faces)

(defcustom gH-org-height t
  "Use varying text heights for org headings."
  :type 'boolean
  :group 'gorgeHousse)

(defcustom gH-invert-matches nil
  "Use inverse video for search matches."
  :type 'boolean
  :group 'gorgeHousse)

(defcustom gH-invert-region t
  "Use inverse video for region."
  :type 'boolean
  :group 'gorgeHousse)

(defcustom gH-transparent-background nil
  "Sets black background color to nil in terminal."
  :type 'boolean
  :group 'gorgeHousse)

;;;; Scheme colors
(defcustom gH-black "#0F1019"
  "gorgeHousse Palette - black."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-red "#D83441"
  "gorgeHousse Palette - red."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-green "#79D836"
  "gorgeHousse Palette - green."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-yellow "#D8B941"
  "gorgeHousse Palette - yellow."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-blue "#3679D8"
  "gorgeHousse Palette - blue."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-magenta "#8041D8"
  "gorgeHousse Palette - magenta."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-cyan "#36D8BD"
  "gorgeHousse Palette - cyan."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-white "#959EA5"
  "gorgeHousse Palette - white."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-bright-black "#2D304C"
  "gorgeHousse Palette - bright black."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-bright-red "#FF7266"
  "gorgeHousse Palette - bright red."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-bright-green "#B2FF66"
  "gorgeHousse Palette - bright green."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-bright-yellow "#FFDF66"
  "gorgeHousse Palette - bright yellow."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-bright-blue "#66A5FF"
  "gorgeHousse Palette - bright blue."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-bright-magenta "#9866FF"
  "gorgeHousse Palette - bright magenta."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-bright-cyan "#66FFD8"
  "gorgeHousse Palette - bright cyan."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-bright-white "#E5F4FF"
  "gorgeHousse Palette - bright white."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-orange "#D85F00"
  "gorgeHousse Palette xterm 166 - orange."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-bright-orange "#FF8700"
  "gorgeHousse Palette xterm 208 - bright orange."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-darker-gray "#1E1E33"
  "gorgeHousse Palette xterm ~17ish - darker gray."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-hard-black "#121212"
  "gorgeHousse Palette xterm 233 - hard black."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-gray1 "#262626"
  "gorgeHousse Palette xterm 235 - gray 1."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-gray2 "#303030"
  "gorgeHousse Palette xterm 236 - gray 2."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-gray3 "#3A3A3A"
  "gorgeHousse Palette xterm 237 - gray 3."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-gray4 "#444444"
  "gorgeHousse Palette xterm 238 - gray 4."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-gray5 "#4E4E4E"
  "gorgeHousse Palette xterm 239 - gray 5."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-dark-red "#5f0000"
  "gorgeHousse Palette xterm 52."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-dark-green "#005f00"
  "gorgeHousse Palette xterm 22."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-dark-cyan "#005f5f"
  "gorgeHousse Palette xterm 23."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-dark-blue "#00005f"
  "gorgeHousse Palette xterm 17."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-foreground "#CEDBE5"
  "gorgeHousse Palette - Foreground."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-background "#0D0E16"
  "gorgeHousse Palette - Background."
  :type 'string
  :group 'gorgeHousse)

(defcustom gH-selection "#175178"
  "gorgeHousse Palette - Selection."
  :type 'string
  :group 'gorgeHousse)

;;;; Set faces colors
(let* ((gH-class '((class color) (min-colors 257)))

       (gH-256-class '((class color) (min-colors 89)))

       (gH-256-black          "black")
       (gH-256-red            "red")
       (gH-256-green          "green")
       (gH-256-yellow         "yellow")
       (gH-256-blue           "blue")
       (gH-256-magenta        "magenta")
       (gH-256-cyan           "cyan")
       (gH-256-white          "white")
       (gH-256-bright-black   "brightblack")
       (gH-256-bright-red     "brightred")
       (gH-256-bright-green   "brightgreen")
       (gH-256-bright-yellow  "brightyellow")
       (gH-256-bright-blue    "brightblue")
       (gH-256-bright-magenta "brightmagenta")
       (gH-256-bright-cyan    "brightcyan")
       (gH-256-bright-white   "brightwhite")

       (gH-256-orange         "color-166")
       (gH-256-bright-orange  "color-208")
       (gH-256-hard-black     "color-233")
       (gH-256-darker-gray    "color-17")
       (gH-256-gray1          "color-235")
       (gH-256-gray2          "color-236")
       (gH-256-gray3          "color-237")
       (gH-256-gray4          "color-238")
       (gH-256-gray5          "color-239")

       (gH-256-dark-red       "color-52")
       (gH-256-dark-green     "color-22")
       (gH-256-dark-cyan      "color-23")
       (gH-256-dark-blue      "color-17"))

  (custom-theme-set-faces
   'gorgeHousse

;;;;; basics
   `(cursor
     ((,gH-class (:background ,gH-white :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-white :foreground ,gH-256-black))))

   `(custom-button
     ((,gH-class (:background ,gH-black :foreground ,gH-bright-white :box (:line-width 2 :style released-button)))
      (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black) :foreground ,gH-256-bright-white :box (:line-width 2 :style released-button)))))

   `(default
      ((,gH-class (:background ,gH-background :foreground ,gH-foreground))
       (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black) :foreground ,gH-256-bright-white))))

   `(default-italic
      ((,gH-class (:italic t))
       (,gH-256-class (:italic t))))

   `(error
     ((,gH-class (:foreground ,gH-red :background ,gH-background :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :weight bold))))

   `(eval-sexp-fu-flash
     ((,gH-class (:background ,gH-green))
      (,gH-256-class (:background ,gH-256-green))))

   `(eval-sexp-fu-flash-error
     ((,gH-class (:background ,gH-red))
      (,gH-256-class (:background ,gH-256-red))))

   `(font-lock-builtin-face
     ((,gH-class (:foreground ,gH-blue :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-blue (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-comment-face
     ((,gH-class (:foreground ,gH-white :background ,gH-background :italic t))
      (,gH-256-class (:foreground ,gH-256-white (:background ,(if gH-transparent-background nil gH-256-black) :italic t)))))

   `(font-lock-constant-face
     ((,gH-class (:foreground ,gH-bright-magenta :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-bright-magenta (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-reference-face
     ((,gH-class (:foreground ,gH-bright-blue :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-bright-blue (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-doc-face
     ((,gH-class (:foreground ,gH-green :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-green (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-function-name-face
     ((,gH-class (:foreground ,gH-yellow :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-yellow (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-keyword-face
     ((,gH-class (:foreground ,gH-red :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-red (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-negation-char-face
     ((,gH-class (:foreground ,gH-bright-magenta :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-bright-magenta (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-preprocessor-face
     ((,gH-class (:foreground ,gH-yellow :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-yellow (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-string-face
     ((,gH-class (:foreground ,gH-bright-green :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-bright-green (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-type-face
     ((,gH-class (:foreground ,gH-bright-blue :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-bright-blue (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-variable-name-face
     ((,gH-class (:foreground ,gH-cyan :background ,gH-background))
      (,gH-256-class (:foreground ,gH-256-cyan (:background ,(if gH-transparent-background nil gH-256-black))))))

   `(font-lock-warning-face
     ((,gH-class (:foreground ,gH-bright-orange :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-bright-orange :background ,(if gH-transparent-background nil gH-256-black)))))

   `(fringe
     ((,gH-class (:foreground ,gH-bright-white :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(header-line
     ((,gH-class (:background ,gH-black))
      (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black)))))

   `(highlight
     ((,gH-class ,(if gH-invert-matches
			       `(:inverse-video t)
			     `(:background ,gH-selection :weight bold)))
      (,gH-256-class ,(if gH-invert-matches
				   `(:inverse-video t)
				 `(:background ,gH-256-dark-blue :weight bold)))))

   `(hl-line
     ((,gH-class (:background ,gH-darker-gray))
      (,gH-256-class (:background ,gH-256-darker-gray))))

   `(isearch
     ((,gH-class ,(if gH-invert-matches
			       `(:inverse-video t :underline t :weight bold)
			     `(:underline t :background ,gH-bright-black :weight bold)))
      (,gH-256-class ,(if gH-invert-matches
				   `(:inverse-video t :underline t :weight bold)
				 `(:underline t :background ,gH-256-bright-black :weight bold)))))
   `(isearch-fail
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(lazy-highlight
     ((,gH-class ,(if gH-invert-matches
			       `(:inverse-video t)
			     `(:background ,gH-bright-black :weight bold)))
      (,gH-256-class ,(if gH-invert-matches
				   `(:inverse-video t)
				 `(:background ,gH-256-bright-black :weight bold)))))

   `(link
     ((,gH-class (:inherit font-lock-comment-face :underline t))
      (,gH-256-class (:inherit font-lock-comment-face :underline t))))

   `(link-visited
     ((,gH-class (:inherit font-lock-comment-face :underline t))
      (,gH-256-class (:inherit font-lock-comment-face :underline t))))

   `(match
     ((,gH-class ,(if gH-invert-matches
			       `(:inverse-video t)
			     `(:background ,gH-bright-black :weight bold)))
      (,gH-256-class ,(if gH-invert-matches
				   `(:inverse-video t)
				 `(:background ,gH-256-bright-black :weight bold)))))

   `(minibuffer-prompt
     ((,gH-class (:weight bold :foreground ,gH-yellow))
      (,gH-256-class (:weight bold :foreground ,gH-256-yellow))))

   `(page-break-lines
     ((,gH-class (:foreground ,gH-gray3))
      (,gH-256-class (:foreground ,gH-256-gray3))))

   `(region
     ((,gH-class ,(if gH-invert-region
			       `(:inverse-video t)
			     `(:background ,gH-hard-black :weight bold)))
      (,gH-256-class ,(if gH-invert-region
				   `(:inverse-video t)
				 `(:background ,gH-256-hard-black :weight bold)))))

   `(secondary-selection
     ((,gH-class (:background ,gH-gray2))
      (,gH-256-class (:background ,gH-256-gray2))))

   `(success
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(tooltip
     ((,gH-class (:background ,gH-blue :foreground ,gH-foreground :bold nil :italic nil :underline nil))
      (,gH-256-class (:background ,gH-256-blue :foreground ,gH-256-bright-white :bold nil :italic nil :underline nil))))

   `(vertical-border
     ((,gH-class (:foreground ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-magenta))))

   `(warning
     ((,gH-class (:foreground ,gH-bright-orange))
      (,gH-256-class (:foreground ,gH-256-bright-orange))))

   `(tool-bar
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))


;;;;; ahs
   `(ahs-face
     ((,gH-class (:background ,gH-magenta))
      (,gH-256-class (:background ,gH-256-magenta))))

   `(ahs-plugin-whole-buffer-face
     ((,gH-class (:background ,gH-yellow :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-yellow :foreground ,gH-256-black))))

   `(ahs-edit-mode-face
     ((,gH-class (:background ,gH-bright-red :foreground ,gH-bright-white))
      (,gH-256-class (:background ,gH-256-bright-red :foreground ,gH-256-bright-white))))

;;;;; anzu-mode
   `(anzu-mode-line
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

;;;;; auto-complete
   `(ac-completion-face
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-white))))

;;;;; avy
   `(avy-lead-face
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-bright-magenta))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-magenta))))

   `(avy-lead-face-0
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-bright-yellow))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-yellow))))

   `(avy-lead-face-1
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-bright-green))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-green))))

   `(avy-lead-face-2
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-bright-blue))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-blue))))


;;;;; cider
   `(cider-enlightened
     ((,gH-class (:background nil :box (:color ,gH-yellow :line-width -1 :style nil) :foreground ,gH-yellow))
      (,gH-256-class (:background nil :box (:color ,gH-256-yellow :line-width -1 :style nil) :foreground ,gH-256-yellow))))

   `(cider-enlightened-face
     ((,gH-class (:background nil :box (:color ,gH-white :line-width -1 :style nil) :foreground ,gH-blue))
      (,gH-256-class (:background nil :box (:color ,gH-256-white :line-width -1 :style nil) :foreground ,gH-256-blue))))

   `(cider-enlightened-local
     ((,gH-class (:foreground ,gH-bright-yellow))
      (,gH-256-class (:foreground ,gH-256-bright-yellow))))

   `(cider-instrumented-face
     ((,gH-class (:background nil :box (:color ,gH-red :line-width -1 :style nil) :foreground ,gH-red))
      (,gH-256-class (:background nil :box (:color ,gH-256-red :line-width -1 :style nil) :foreground ,gH-256-red))))

   `(cider-result-overlay-face
     ((,gH-class (:background nil :box (:color ,gH-blue :line-width -1 :style nil) :foreground ,gH-blue))
      (,gH-256-class (:background nil :box (:color ,gH-256-blue :line-width -1 :style nil) :foreground ,gH-256-blue))))

   `(cider-test-error-face
     ((,gH-class (:background ,gH-bright-orange :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-orange :foreground ,gH-256-black))))

   `(cider-test-failure-face
     ((,gH-class (:background ,gH-red :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-red :foreground ,gH-256-bright-white))))

   `(cider-test-success-face
     ((,gH-class (:background ,gH-green :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-green :foreground ,gH-256-black))))

   `(cider-traced-face
     ((,gH-class :box (:color ,gH-cyan :line-width -1 :style nil))
      (,gH-256-class :box (:color ,gH-256-cyan :line-width -1 :style nil))))

   `(cider-fringe-good-face
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

   `(cider-fragile-button-face
     ((,gH-class :foreground ,gH-orange :box (:style released-button))
      (,gH-256-class :foreground ,gH-256-orange :box (:style released-button))))

   `(cider-stacktrace-promoted-button-face
     ((,gH-class :foreground ,gH-red :box (:style released-button))
      (,gH-256-class :foreground ,gH-256-red :box (:style released-button))))

   `(cider-stacktrace-suppressed-button-face
     ((,gH-class :foreground ,gH-white :box (:style pressed-button))
      (,gH-256-class :foreground ,gH-256-white :box (:style pressed-button))))

   `(cider-enlightened-local-face
     ((,gH-class :foreground ,gH-yellow :weight bold)
      (,gH-256-class :foreground ,gH-256-yellow :weight bold)))

   `(cider-deprecated-face
     ((,gH-class  :foreground ,gH-bright-yellow :underline t)
      (,gH-256-class :foreground ,gH-256-bright-yellow :underline t)))

   `(cider-debug-code-overlay-face
     ((,gH-class :background ,gH-bright-blue :foreground ,gH-black)
      (,gH-256-class :background ,gH-256-bright-blue :foreground ,gH-256-black)))

   `(cider-docview-table-border-face
     ((,gH-class :foreground ,gH-white)
      (,gH-256-class :foreground ,gH-256-white)))

;;;;; clojure
   `(clojure-keyword-face
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

;;;;; company
   `(company-echo-common
     ((,gH-class (:background ,gH-foreground :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-white :foreground ,gH-256-black))))

   `(company-preview
     ((,gH-class (:background ,gH-gray1 :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-gray1 :foreground ,gH-256-bright-white))))

   `(company-preview-common
     ((,gH-class (:background ,gH-gray1 :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-gray1 :foreground ,gH-256-bright-white))))

   `(company-preview-search
     ((,gH-class (:inherit match))
      (,gH-256-class (:inherit match))))

   `(company-scrollbar-bg
     ((,gH-class (:background ,gH-gray1))
      (,gH-256-class (:background ,gH-256-gray1))))

   `(company-scrollbar-fg
     ((,gH-class (:background ,gH-white))
      (,gH-256-class (:background ,gH-256-white))))

   `(company-template-field
     ((,gH-class (:inherit region))
      (,gH-256-class (:inherit region))))

   `(company-tooltip
     ((,gH-class (:background ,gH-gray1 :foreground ,gH-white))
      (,gH-256-class (:background ,gH-256-gray1 :foreground ,gH-256-white))))

   `(company-tooltip-annotation
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(company-tooltip-common
     ((,gH-class (:background ,gH-gray1 :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-gray1 :foreground ,gH-256-bright-white))))

   `(company-tooltip-common-selection
     ((,gH-class (:foreground ,gH-bright-magenta))
      (,gH-256-class (:foreground ,gH-256-bright-magenta))))

   `(company-tooltip-mouse
     ((,gH-class (:inherit highlight))
      (,gH-256-class (:inherit highlight))))

   `(company-tooltip-search
     ((,gH-class (:inherit match))
      (,gH-256-class (:inherit match))))

   `(company-tooltip-selection
     ((,gH-class (:foreground ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-magenta))))

;;;;; racer
   `(racer-tooltip
     ((,gH-class (:foreground ,gH-foreground :background ,gH-gray1))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,gH-256-gray1))))

   `(racer-help-heading-face
     ((,gH-class (:foreground ,gH-foreground :weight bold))
      (,gH-256-class (:foreground ,gH-256-bright-white :weight bold))))

;;;;; rust
   `(rust-builtin-formatting-macro-face
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(rust-question-mark-face
     ((,gH-class (:foreground ,gH-blue :weight bold))
      (,gH-256-class (:foreground ,gH-256-blue :weight bold))))

   `(rust-string-interpolation-face
     ((,gH-class (:foreground ,gH-bright-green :italic t))
      (,gH-256-class (:foreground ,gH-256-bright-green :italic t))))

   `(rust-unsafe-face
     ((,gH-class (:foreground ,gH-bright-orange))
      (,gH-256-class (:foreground ,gH-256-bright-orange))))

;;;;; diff
   `(diff-added
     ((,gH-class :background nil :foreground ,gH-green)
      (,gH-256-class :background nil :foreground ,gH-256-green)))

   `(diff-changed
     ((,gH-class :background nil :foreground ,gH-red)
      (,gH-256-class :background nil :foreground ,gH-256-red)))

   `(diff-header
     ((,gH-class :background ,gH-bright-black :foreground ,gH-yellow)
      (,gH-256-class :background ,gH-256-bright-black :foreground ,gH-256-yellow)))

   `(diff-indicator-added
     ((,gH-class :background nil :foreground ,gH-green)
      (,gH-256-class :background nil :foreground ,gH-256-green)))

   `(diff-indicator-changed
     ((,gH-class :background nil :foreground ,gH-red)
      (,gH-256-class :background nil :foreground ,gH-256-red)))

   `(diff-indicator-removed
     ((,gH-class :background nil :foreground ,gH-red)
      (,gH-256-class :background nil :foreground ,gH-256-red)))

   `(diff-refine-added
     ((,gH-class :background ,gH-green :foreground ,gH-black)
      (,gH-256-class :background ,gH-256-green :foreground ,gH-256-black)))

   `(diff-refine-changed
     ((,gH-class :background ,gH-blue :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-blue :foreground ,gH-256-bright-white)))

   `(diff-refine-removed
     ((,gH-class :background ,gH-red :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-red :foreground ,gH-256-bright-white)))

   `(diff-removed
     ((,gH-class :background nil :foreground ,gH-red)
      (,gH-256-class :background nil :foreground ,gH-256-red)))

;;;;; diff-hl
   `(diff-hl-change
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(diff-hl-delete
     ((,gH-class :foreground ,gH-red)
      (,gH-256-class :foreground ,gH-256-red)))

   `(diff-hl-insert
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

;;;;; dired
   `(dired-directory
     ((,gH-class (:foreground ,gH-blue :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-blue :background ,(if gH-transparent-background nil gH-256-black)))))

   `(dired-flagged
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(dired-header
     ((,gH-class (:foreground ,gH-green :weight bold))
      (,gH-256-class (:foreground ,gH-256-green :weight bold))))

   `(dired-ignored
     ((,gH-class (:inherit shadow))
      (,gH-256-class (:inherit shadow))))

   `(dired-mark
     ((,gH-class (:foreground ,gH-green :weight bold))
      (,gH-256-class (:foreground ,gH-256-green :weight bold))))

   `(dired-marked
     ((,gH-class (:foreground ,gH-magenta :weight bold))
      (,gH-256-class (:foreground ,gH-256-magenta :weight bold))))

   `(dired-perm-write
     ((,gH-class (:foreground ,gH-foreground :underline t))
      (,gH-256-class (:foreground ,gH-256-bright-white :underline t))))

   `(dired-symlink
     ((,gH-class (:foreground ,gH-cyan :background ,gH-black :weight bold))
      (,gH-256-class (:foreground ,gH-256-cyan :background ,(if gH-transparent-background nil gH-256-black) :weight bold))))

   `(dired-warning
     ((,gH-class (:foreground ,gH-bright-orange))
      (,gH-256-class (:foreground ,gH-256-bright-orange))))

   `(diredp-date-time
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(diredp-number
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(diredp-file-name
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(diredp-file-suffix
     ((,gH-class (:foreground ,gH-bright-blue))
      (,gH-256-class (:foreground ,gH-256-bright-blue))))

   `(diredp-dir-heading
     ((,gH-class (:foreground ,gH-foreground :underline t))
      (,gH-256-class (:foreground ,gH-256-bright-white :underline t))))

   `(diredp-dir-heading
     ((,gH-class (:foreground ,gH-foreground :underline t))
      (,gH-256-class (:foreground ,gH-256-bright-white :underline t))))

   `(diredp-dir-priv
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(diredp-read-priv
     ((,gH-class (:foreground ,gH-bright-yellow))
      (,gH-256-class (:foreground ,gH-256-bright-yellow))))

   `(diredp-write-priv
     ((,gH-class (:foreground ,gH-bright-red))
      (,gH-256-class (:foreground ,gH-256-bright-red))))

   `(diredp-write-priv
     ((,gH-class (:foreground ,gH-bright-red))
      (,gH-256-class (:foreground ,gH-256-bright-red))))

   `(diredp-dir-name
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(diredp-exec-priv
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(diredp-symlink
     ((,gH-class (:foreground ,gH-bright-cyan))
      (,gH-256-class (:foreground ,gH-256-bright-cyan))))

   `(diredp-tagged-autofile-name
     ((,gH-class (:foreground ,gH-foreground :background ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,gH-256-magenta))))

   `(diredp-no-priv
     ((,gH-class (:foreground ,gH-white))
      (,gH-256-class (:foreground ,gH-256-white))))

   `(diredp-flag-mark
     ((,gH-class (:background ,gH-green :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-green :foreground ,gH-256-black))))

   `(diredp-flag-mark-line
     ((,gH-class (:background ,gH-green :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-green :foreground ,gH-256-black))))

   `(diredp-autofile-name
     ((,gH-class (:background ,gH-blue :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-blue :foreground ,gH-256-bright-white))))

   `(diredp-deletion
     ((,gH-class (:background ,gH-red :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-red :foreground ,gH-256-bright-white))))

   `(diredp-ignored-file-name
     ((,gH-class (:foreground ,gH-white))
      (,gH-256-class (:foreground ,gH-256-white))))

   `(diredp-link-priv
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(diredp-mode-line-marked
     ((,gH-class (:foreground ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-magenta))))

   `(diredp-other-priv
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(diredp-rare-priv
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

;;;;; ediff
   `(ediff-current-diff-A
     ((,gH-class(:background ,gH-dark-red))
      (,gH-256-class(:background ,gH-256-dark-red))))

   `(ediff-current-diff-Ancestor
     ((,gH-class(:background ,gH-dark-cyan))
      (,gH-256-class(:background ,gH-256-dark-cyan))))

   `(ediff-current-diff-B
     ((,gH-class(:background ,gH-dark-green))
      (,gH-256-class(:background ,gH-256-dark-green))))

   `(ediff-current-diff-C
     ((,gH-class(:background ,gH-dark-blue))
      (,gH-256-class(:background ,gH-256-dark-blue))))

   `(ediff-even-diff-A
     ((,gH-class(:background ,gH-gray1))
      (,gH-256-class(:background ,gH-256-gray1))))

   `(ediff-even-diff-Ancestor
     ((,gH-class(:background ,gH-gray1))
      (,gH-256-class(:background ,gH-256-gray1))))

   `(ediff-even-diff-B
     ((,gH-class(:background ,gH-gray1))
      (,gH-256-class(:background ,gH-256-gray1))))

   `(ediff-even-diff-C
     ((,gH-class(:background ,gH-gray1))
      (,gH-256-class(:background ,gH-256-gray1))))

   `(ediff-fine-diff-A
     ((,gH-class(:background ,gH-red :weight bold))
      (,gH-256-class(:background ,gH-256-red :weight bold))))

   `(ediff-fine-diff-Ancestor
     ((,gH-class(:background ,gH-cyan :weight bold))
      (,gH-256-class(:background ,gH-256-cyan :weight bold))))

   `(ediff-fine-diff-B
     ((,gH-class(:background ,gH-green :weight bold))
      (,gH-256-class(:background ,gH-256-green :weight bold))))

   `(ediff-fine-diff-C
     ((,gH-class(:background ,gH-blue :weight bold))
      (,gH-256-class(:background ,gH-256-blue :weight bold))))

   `(ediff-odd-diff-A
     ((,gH-class(:background ,gH-gray2))
      (,gH-256-class(:background ,gH-256-gray2))))

   `(ediff-odd-diff-Ancestor
     ((,gH-class(:background ,gH-gray2))
      (,gH-256-class(:background ,gH-256-gray2))))

   `(ediff-odd-diff-B
     ((,gH-class(:background ,gH-gray2))
      (,gH-256-class(:background ,gH-256-gray2))))

   `(ediff-odd-diff-C
     ((,gH-class(:background ,gH-gray2))
      (,gH-256-class(:background ,gH-256-gray2))))

;;;;; ein
   `(ein:cell-input-area
     ((,gH-class (:background ,gH-bright-black))
      (,gH-256-class (:background ,gH-256-bright-black))))

   `(ein:cell-input-prompt
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(ein:cell-output-prompt
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(ein:notification-tab-normal
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(ein:notification-tab-selected
     ((,gH-class (:foreground ,gH-green :weight bold))
      (,gH-256-class (:foreground ,gH-256-green :weight bold))))

;;;;;eldoc
   `(eldoc-highlight-function-argument
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))


;;;;; enh-ruby
   `(enh-ruby-string-delimiter-face
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(enh-ruby-op-face
     ((,gH-class (:background ,gH-black :foreground ,gH-foreground))
      (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black) :foreground ,gH-256-bright-white))))

;;;;; erc
   `(erc-input-face
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(erc-my-nick-face
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(erc-nick-default-face
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(erc-nick-prefix-face
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(erc-notice-face
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(erc-prompt-face
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

   `(erc-timestamp-face
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))


;;;;; eshell
   `(eshell-ls-archive
     ((,gH-class (:foreground ,gH-red :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :weight bold))))

   `(eshell-ls-backup
     ((,gH-class (:inherit font-lock-comment-face))
      (,gH-256-class (:inherit font-lock-comment-face))))

   `(eshell-ls-clutter
     ((,gH-class (:inherit font-lock-comment-face))
      (,gH-256-class (:inherit font-lock-comment-face))))

   `(eshell-ls-directory
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(eshell-ls-executable
     ((,gH-class (:foreground ,gH-orange :weight bold))
      (,gH-256-class (:foreground ,gH-256-orange :weight bold))))

   `(eshell-ls-missing
     ((,gH-class (:inherit font-lock-warning-face))
      (,gH-256-class (:inherit font-lock-warning-face))))

   `(eshell-ls-product
     ((,gH-class (:inherit font-lock-doc-face))
      (,gH-256-class (:inherit font-lock-doc-face))))

   `(eshell-ls-special
     ((,gH-class (:foreground ,gH-magenta :weight bold))
      (,gH-256-class (:foreground ,gH-256-magenta :weight bold))))

   `(eshell-ls-symlink
     ((,gH-class (:foreground ,gH-cyan :weight bold))
      (,gH-256-class (:foreground ,gH-256-cyan :weight bold))))

   `(eshell-ls-unreadable
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(eshell-prompt
     ((,gH-class (:foreground ,gH-magenta :weight bold))
      (,gH-256-class (:foreground ,gH-256-magenta :weight bold))))

;;;;; evil
   `(evil-ex-substitute-matches
     ((,gH-class (:background ,gH-red :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-red :foreground ,gH-256-bright-white))))

   `(evil-ex-substitute-replacement
     ((,gH-class (:background ,gH-bright-green :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-green :foreground ,gH-256-black))))

   `(evil-search-highlight-persist-highlight-face
     ((,gH-class ,(if gH-invert-matches
			       `(:inverse-video t)
			     `(:background ,gH-bright-black :weight bold)))
      (,gH-256-class ,(if gH-invert-matches
				   `(:inverse-video t)
				 `(:background ,gH-256-bright-black :weight bold)))))

   `(flycheck-error
     ((,gH-class (:foreground ,gH-red :underline t))
      (,gH-256-class (:foreground ,gH-256-red :underline t))))

   `(flycheck-info
     ((,gH-class (:foreground ,gH-foreground :underline t))
      (,gH-256-class (:foreground ,gH-256-bright-white :underline t))))

   `(flycheck-warning
     ((,gH-class (:foreground ,gH-bright-orange :underline t))
      (,gH-256-class (:foreground ,gH-bright-orange :underline t))))

   `(flycheck-error-list-checker-name
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(flycheck-fringe-error
     ((,gH-class (:foreground ,gH-red :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :weight bold))))

   `(flycheck-fringe-info
     ((,gH-class (:foreground ,gH-red :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :weight bold))))

   `(flycheck-fringe-warning
     ((,gH-class (:foreground ,gH-bright-orange :weight bold))
      (,gH-256-class (:foreground ,gH-256-bright-orange :weight bold))))

;;;;; Flyspell
   ;; ------------------------------
   `(flyspell-duplicate
     ((,gH-class (:foreground ,gH-foreground :underline t))
      (,gH-256-class (:foreground ,gH-256-bright-white :underline t))))

   `(flyspell-incorrect
     ((,gH-class (:foreground ,gH-red :underline t))
      (,gH-256-class (:foreground ,gH-256-red :underline t))))

;;;;; jabber
   `(jabber-activity-face
     ((,gH-class (:weight bold :foreground ,gH-red))
      (,gH-256-class (:weight bold :foreground ,gH-256-red))))

   `(jabber-activity-personal-face
     ((,gH-class (:weight bold :foreground ,gH-blue))
      (,gH-256-class (:weight bold :foreground ,gH-256-blue))))

   `(jabber-chat-error
     ((,gH-class (:weight bold :foreground ,gH-red))
      (,gH-256-class (:weight bold :foreground ,gH-256-red))))

   `(jabber-chat-prompt-foreign
     ((,gH-class (:weight bold :foreground ,gH-red))
      (,gH-256-class (:weight bold :foreground ,gH-256-red))))

   `(jabber-chat-prompt-local
     ((,gH-class (:weight bold :foreground ,gH-blue))
      (,gH-256-class (:weight bold :foreground ,gH-256-blue))))

   `(jabber-chat-prompt-system
     ((,gH-class (:weight bold :foreground ,gH-green))
      (,gH-256-class (:weight bold :foreground ,gH-256-green))))

   `(jabber-chat-text-foreign
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(jabber-chat-text-local
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(jabber-rare-time-face
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(jabber-roster-user-away
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(jabber-roster-user-chatty
     ((,gH-class (:weight bold :foreground ,gH-green))
      (,gH-256-class (:weight bold :foreground ,gH-256-green))))

   `(jabber-roster-user-dnd
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(jabber-roster-user-error
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(jabber-roster-user-offline
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(jabber-roster-user-online
     ((,gH-class (:weight bold :foreground ,gH-green))
      (,gH-256-class (:weight bold :foreground ,gH-256-green))))

   `(jabber-roster-user-xa
     ((,gH-class (:foreground ,gH-cyan))
      (,gH-256-class (:foreground ,gH-256-cyan))))

;;;;; git
   `(git-commit-summary
     ((,gH-class (:foreground ,gH-bright-magenta))
      (,gH-256-class (:foreground ,gH-256-bright-magenta))))

   `(git-commit-note
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(git-commit-nonempty-second-line
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(diff-file-header
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(diff-hunk-header
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(diff-function
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(diff-header
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

;;;;; git-gutter-fr
   `(git-gutter-fr:added
     ((,gH-class (:foreground ,gH-green :weight bold))
      (,gH-256-class (:foreground ,gH-256-green :weight bold))))

   `(git-gutter-fr:deleted
     ((,gH-class (:foreground ,gH-red :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :weight bold))))

   `(git-gutter-fr:modified
     ((,gH-class (:foreground ,gH-blue :weight bold))
      (,gH-256-class (:foreground ,gH-256-blue :weight bold))))

   `(git-gutter+-added
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(git-gutter+-deleted
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(git-gutter+-separator
     ((,gH-class (:foreground ,gH-cyan))
      (,gH-256-class (:foreground ,gH-256-cyan))))

   `(git-gutter+-modified
     ((,gH-class (:foreground ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-magenta))))

   `(git-gutter+-unchanged
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(git-gutter:added
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(git-gutter:modified
     ((,gH-class (:foreground ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-magenta))))

   `(git-gutter:unchanged
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

;;;;; git-timemachine
   `(git-timemachine-minibuffer-detail-face
     ((,gH-class (:foreground ,gH-blue :weight bold :background ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue :weight bold :background ,gH-256-blue))))


;;;;; gnus
   `(gnus-emphasis-highlight-words
     ((,gH-class (:background ,gH-green :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-green :foreground ,gH-256-black))))

   `(gnus-header-content
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(gnus-header-from
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(gnus-header-name
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(gnus-header-subject
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

   `(gnus-summary-cancelled
     ((,gH-class (:background ,gH-bright-orange :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-orange :foreground ,gH-256-black))))

;;;;; guide-key
   `(guide-key/highlight-command-face
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(guide-key/key-face
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(guide-key/prefix-command-face
     ((,gH-class (:foreground ,gH-red :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :weight bold))))

;;;;; helm
   `(helm-bookmark-directory
     ((,gH-class (:inherit helm-ff-directory))
      (,gH-256-class (:inherit helm-ff-directory))))

   `(helm-bookmark-file
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(helm-bookmark-gnus
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(helm-bookmark-info
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(helm-bookmark-man
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(helm-bookmark-w3m
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(helm-buffer-directory
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(helm-buffer-file
     ((,gH-class (:foreground ,gH-foreground :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-buffer-not-saved
     ((,gH-class (:foreground ,gH-green :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-green :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-buffer-process
     ((,gH-class (:foreground ,gH-red :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-red :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-buffer-saved-out
     ((,gH-class (:foreground ,gH-foreground :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-buffer-size
     ((,gH-class (:foreground ,gH-foreground :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-candidate-number
     ((,gH-class (:background ,gH-black :foreground ,gH-red :weight bold))
      (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black) :foreground ,gH-256-red :weight bold))))

   `(helm-ff-directory
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(helm-ff-dotted-directory
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(helm-ff-dotted-symlink-directory
     ((,gH-class (:foreground ,gH-cyan))
      (,gH-256-class (:foreground ,gH-256-cyan))))

   `(helm-ff-executable
     ((,gH-class (:foreground ,gH-green :background ,gH-black :weight normal))
      (,gH-256-class (:foreground ,gH-256-green :background ,(if gH-transparent-background nil gH-256-black) :weight normal))))

   `(helm-ff-file
     ((,gH-class (:foreground ,gH-foreground :background ,gH-black :weight normal))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black) :weight normal))))

   `(helm-ff-invalid-symlink
     ((,gH-class (:foreground ,gH-red :background ,gH-black :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :background ,(if gH-transparent-background nil gH-256-black) :weight bold))))

   `(helm-ff-prefix
     ((,gH-class (:foreground ,gH-black :background ,gH-red :weight normal))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-red :weight normal))))

   `(helm-ff-symlink
     ((,gH-class (:foreground ,gH-cyan :background ,gH-black :weight bold))
      (,gH-256-class (:foreground ,gH-256-cyan :background ,(if gH-transparent-background nil gH-256-black) :weight bold))))

   `(helm-grep-cmd-line
     ((,gH-class (:foreground ,gH-foreground :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-grep-file
     ((,gH-class (:foreground ,gH-foreground :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-grep-finish
     ((,gH-class (:foreground ,gH-foreground :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-grep-lineno
     ((,gH-class (:foreground ,gH-bright-blue :background ,gH-black :weight bold))
      (,gH-256-class (:foreground ,gH-256-bright-blue :background ,(if gH-transparent-background nil gH-256-black) :weight bold))))

   `(helm-grep-match
     ((,gH-class (:foreground nil :background nil :inherit helm-match))
      (,gH-256-class (:foreground nil :background nil :inherit helm-match))))

   `(helm-header
     ((,gH-class (:foreground ,gH-foreground :background ,gH-black :underline nil :box nil))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black) :underline nil :box nil))))

   `(helm-header-line-left-margin
     ((,gH-class (:foreground ,gH-red :background ,nil))
      (,gH-256-class (:foreground ,gH-256-red :background ,nil))))

   `(helm-match
     ((,gH-class (:foreground ,gH-bright-blue))
      (,gH-256-class (:foreground ,gH-256-bright-blue))))

   `(helm-match-item
     ((,gH-class (:foreground ,gH-bright-blue))
      (,gH-256-class (:foreground ,gH-256-bright-blue))))

   `(helm-moccur-buffer
     ((,gH-class (:foreground ,gH-blue :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-blue :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-selection
     ((,gH-class (:background ,gH-darker-gray :weight bold))
      (,gH-256-class (:background ,gH-256-darker-gray :weight bold))))

   `(helm-selection-line
     ((,gH-class (:background ,gH-darker-gray :weight bold))
      (,gH-256-class (:background ,gH-256-darker-gray :weight bold))))

   `(helm-separator
     ((,gH-class (:foreground ,gH-green :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-green :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-source-header
     ((,gH-class (:background ,gH-black :foreground ,gH-green :underline t))
      (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black) :foreground ,gH-256-green :underline t))))

   `(helm-time-zone-current
     ((,gH-class (:foreground ,gH-red :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-red :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-time-zone-home
     ((,gH-class (:foreground ,gH-green :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-green :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-visible-mark
     ((,gH-class (:foreground ,gH-red :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-red :background ,(if gH-transparent-background nil gH-256-black)))))


;;;;; helm-swoop
   `(helm-swoop-target-line-block-face
     ((,gH-class (:foreground ,gH-yellow :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-yellow :background ,(if gH-transparent-background nil gH-256-black)))))

   `(helm-swoop-target-line-face
     ((,gH-class (:background ,gH-darker-gray :weight bold))
      (,gH-256-class (:background ,gH-256-darker-gray :weight bold))))

   `(helm-swoop-target-word-face
     ((,gH-class (:foreground ,gH-magenta :underline t))
      (,gH-256-class (:foreground ,gH-256-magenta :underline t))))

;;;;; highlights
   `(hi-yellow
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(hi-green
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

;;;;; highlight-indentation
   `(highlight-indentation-face
     ((,gH-class (:background ,gH-white))
      (,gH-256-class (:background ,gH-256-white))))

;;;;; highlight-symbol
   `(highlight-symbol-face
     ((,gH-class (:background ,gH-bright-black))
      (,gH-256-class (:background ,gH-256-bright-black))))

;;;;; hydra
   `(hydra-face-blue
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(hydra-face-red
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

;;;;; ido
   `(ido-first-match
     ((,gH-class (:foreground ,gH-green :weight bold))
      (,gH-256-class (:foreground ,gH-256-green :weight bold))))

   `(ido-only-match
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

   `(ido-subdir
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(ido-indicator
     ((,gH-class (:background ,gH-red :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-red :foreground ,gH-256-bright-white))))

   `(ido-vertical-match-face
     ((,gH-class (:foreground ,gH-green :underline nil))
      (,gH-256-class (:foreground ,gH-256-green :underline nil))))

;;;;; info
   `(info-header-xref
     ((,gH-class (:foreground ,gH-yellow :underline t))
      (,gH-256-class (:foreground ,gH-256-yellow :underline t))))

   `(info-menu
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(info-node
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

   `(info-quoted-name
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(info-reference-item
     ((,gH-class (:background nil :underline t :weight bold))
      (,gH-256-class (:background nil :underline t :weight bold))))

   `(info-string
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(info-title-1
     ((,gH-class (:height 1.4 :weight bold))
      (,gH-256-class (:height 1.4 :weight bold))))

   `(info-title-2
     ((,gH-class (:height 1.3 :weight bold))
      (,gH-256-class (:height 1.3 :weight bold))))

   `(info-title-3
     ((,gH-class (:height 1.3))
      (,gH-256-class (:height 1.3))))

   `(info-title-4
     ((,gH-class (:height 1.2))
      (,gH-256-class (:height 1.2))))

;;;;; ivy
   `(ivy-current-match
     ((,gH-class (:background ,gH-bright-black :weight bold))
      (,gH-256-class (:background ,gH-256-bright-black :weight bold))))

   `(ivy-minibuffer-match-face-1
     ((,gH-class (:weight bold))
      (,gH-256-class (:weight bold))))

   `(ivy-minibuffer-match-face-2
     ((,gH-class (:foreground ,gH-magenta :underline t))
      (,gH-256-class (:foreground ,gH-256-magenta :underline t))))

   `(ivy-minibuffer-match-face-3
     ((,gH-class (:foreground ,gH-yellow :underline t))
      (,gH-256-class (:foreground ,gH-256-yellow :underline t))))

   `(ivy-minibuffer-match-face-4
     ((,gH-class (:foreground ,gH-bright-green :underline t))
      (,gH-256-class (:foreground ,gH-256-bright-green :underline t))))

   `(ivy-remote
     ((,gH-class (:foreground ,gH-cyan))
      (,gH-256-class (:foreground ,gH-256-cyan))))

   `(ivy-modified-buffer
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(ivy-modified-outside-buffer
     ((,gH-class (:foreground ,gH-bright-red))
      (,gH-256-class (:foreground ,gH-256-bright-red))))

;;;;; latex
   `(font-latex-bold-face
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(font-latex-italic-face
     ((,gH-class (:foreground ,gH-red :italic t))
      (,gH-256-class (:foreground ,gH-256-red :italic t))))

   `(font-latex-match-reference-keywords
     ((,gH-class (:foreground ,gH-bright-magenta))
      (,gH-256-class (:foreground ,gH-256-bright-magenta))))

   `(font-latex-match-variable-keywords
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(font-latex-sectioning-0-face
     ((,gH-class (:weight bold :foreground ,gH-bright-green :height ,(if gH-org-height 1.3 1.0)))
      (,gH-256-class (:weight bold :foreground ,gH-256-bright-green :height ,(if gH-org-height 1.3 1.0)))))

   `(font-latex-sectioning-1-face
     ((,gH-class (:weight bold :foreground ,gH-bright-yellow :height ,(if gH-org-height 1.3 1.0)))
      (,gH-256-class (:weight bold :foreground ,gH-256-bright-yellow :height ,(if gH-org-height 1.3 1.0)))))

   `(font-latex-sectioning-2-face
     ((,gH-class (:weight bold :foreground ,gH-blue :height ,(if gH-org-height 1.3 1.0)))
      (,gH-256-class (:weight bold :foreground ,gH-256-blue :height ,(if gH-org-height 1.3 1.0)))))

   `(font-latex-sectioning-3-face
     ((,gH-class (:weight bold :foreground ,gH-cyan :height ,(if gH-org-height 1.2 1.0)))
      (,gH-256-class (:weight bold :foreground ,gH-256-cyan :height ,(if gH-org-height 1.2 1.0)))))

   `(font-latex-sectioning-4-face
     ((,gH-class (:bold nil :foreground ,gH-bright-green :height ,(if gH-org-height 1.1 1.0)))
      (,gH-class (:bold nil :foreground ,gH-256-bright-green :height ,(if gH-org-height 1.1 1.0)))))

   `(font-latex-sectioning-5-face
     ((,gH-class (:bold nil :foreground ,gH-yellow))
      (,gH-256-class (:bold nil :foreground ,gH-256-yellow))))

   `(font-latex-string-face
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

;;;;; Line numbers
   `(linum
     ((,gH-class (:foreground ,gH-white :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(linum-relative-current-face
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(line-number
     ((,gH-class (:foreground ,gH-white :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(line-number-current-line
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

;;;;; Git
   `(diff-context
     ((,gH-class :foreground ,gH-white)
      (,gH-256-class :foreground ,gH-256-white)))

;;;;; magit
   `(magit-blame-culprit
     ((,gH-class :foreground ,gH-yellow)
      (,gH-256-class :foreground ,gH-256-yellow)))

   `(magit-blame-header
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

   `(magit-blame-sha1
     ((,gH-class :foreground ,gH-yellow)
      (,gH-256-class :foreground ,gH-256-yellow)))

   `(magit-blame-subject
     ((,gH-class :foreground ,gH-yellow)
      (,gH-256-class :foreground ,gH-256-yellow)))

   `(magit-blame-time
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

   `(magit-blame-name
     ((,gH-class :foreground ,gH-yellow)
      (,gH-256-class :foreground ,gH-256-yellow)))

   `(magit-blame-heading
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

   `(magit-blame-hash
     ((,gH-class :foreground ,gH-yellow)
      (,gH-256-class :foreground ,gH-256-yellow)))

   `(magit-blame-summary
     ((,gH-class :foreground ,gH-yellow)
      (,gH-256-class :foreground ,gH-256-yellow)))

   `(magit-blame-date
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

   `(magit-log-date
     ((,gH-class :foreground ,gH-foreground)
      (,gH-256-class :foreground ,gH-256-bright-white)))

   `(magit-log-graph
     ((,gH-class :foreground ,gH-foreground)
      (,gH-256-class :foreground ,gH-256-bright-white)))

   `(magit-reflog-amend
     ((,gH-class :foreground ,gH-magenta)
      (,gH-256-class :foreground ,gH-256-magenta)))

   `(magit-reflog-other
     ((,gH-class :foreground ,gH-cyan)
      (,gH-256-class :foreground ,gH-256-cyan)))

   `(magit-reflog-rebase
     ((,gH-class :foreground ,gH-magenta)
      (,gH-256-class :foreground ,gH-256-magenta)))

   `(magit-reflog-remote
     ((,gH-class :foreground ,gH-cyan)
      (,gH-256-class :foreground ,gH-256-cyan)))

   `(magit-reflog-reset
     ((,gH-class :foreground ,gH-red)
      (,gH-256-class :foreground ,gH-256-red)))

   `(magit-branch
     ((,gH-class (:foreground ,gH-bright-magenta :weight bold))
      (,gH-256-class (:foreground ,gH-256-bright-magenta :weight bold))))

   `(magit-branch-current
     ((,gH-class (:background ,gH-black :foreground ,gH-blue :weight bold :box t))
      (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black) :foreground ,gH-256-blue :weight bold :box t))))

   `(magit-branch-local
     ((,gH-class (:background ,gH-black :foreground ,gH-blue :weight bold))
      (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black) :foreground ,gH-256-blue :weight bold))))

   `(magit-branch-remote
     ((,gH-class (:background ,gH-black :foreground ,gH-orange :weight bold))
      (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black) :foreground ,gH-256-orange :weight bold))))

   `(magit-diff-file-header
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(magit-diff-file-heading
     ((,gH-class (:foreground ,gH-blue :weight light))
      (,gH-256-class (:foreground ,gH-256-blue :weight light))))

   `(magit-diff-file-heading-highlight
     ((,gH-class (:foreground ,gH-blue :weight bold))
      (,gH-256-class (:foreground ,gH-256-blue :weight bold))))

   `(magit-diff-file-heading-selection
     ((,gH-class (:foreground ,gH-blue :weight bold :background ,gH-darker-gray))
      (,gH-256-class (:foreground ,gH-256-blue :weight bold :background ,gH-256-darker-gray))))


   `(magit-diff-hunk-heading
     ((,gH-class (:foreground ,gH-yellow :weight light))
      (,gH-256-class (:foreground ,gH-256-yellow :weight light))))

   `(magit-diff-hunk-heading-highlight
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

   `(magit-diff-hunk-heading-selection
     ((,gH-class (:foreground ,gH-black :background ,gH-selection :weight bold))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-white :weight bold))))


   `(magit-diff-added
     ((,gH-class (:foreground ,gH-green :weight light))
      (,gH-256-class (:foreground ,gH-256-green :weight light))))

   `(magit-diff-removed
     ((,gH-class (:foreground ,gH-red :weight light))
      (,gH-256-class (:foreground ,gH-256-red :weight light))))

   `(magit-diff-context
     ((,gH-class (:foreground ,gH-white :weight light))
      (,gH-256-class (:foreground ,gH-256-white :weight light))))

   `(magit-diff-added-highlight
     ((,gH-class (:foreground ,gH-green :weight bold))
      (,gH-256-class (:foreground ,gH-256-green :weight bold))))

   `(magit-diff-removed-highlight
     ((,gH-class (:foreground ,gH-red :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :weight bold))))

   `(magit-diff-context-highlight
     ((,gH-class (:foreground ,gH-white :weight bold))
      (,gH-256-class (:foreground ,gH-256-white :weight bold))))

   `(magit-diff-base
     ((,gH-class (:foreground ,gH-white :weight light))
      (,gH-256-class (:foreground ,gH-256-white :weight light))))

   `(magit-diff-base-highlight
     ((,gH-class (:foreground ,gH-white :weight bold))
      (,gH-256-class (:foreground ,gH-256-white :weight bold))))

   `(magit-diff-lines-boundary
     ((,gH-class (:background ,gH-white :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-white :foreground ,gH-256-black))))

   `(magit-diff-lines-heading
     ((,gH-class (:background ,gH-white :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-white :foreground ,gH-256-black))))

   `(magit-hash
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(magit-item-highlight
     ((,gH-class :background ,gH-bright-black)
      (,gH-256-class :background ,gH-256-bright-black)))

   `(magit-log-author
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(magit-log-head-label-head
     ((,gH-class (:background ,gH-yellow :foreground ,gH-black :weight bold))
      (,gH-256-class (:background ,gH-256-yellow :foreground ,gH-256-black :weight bold))))

   `(magit-log-head-label-local
     ((,gH-class (:background ,gH-red :foreground ,gH-black :weight bold))
      (,gH-256-class (:background ,gH-256-red :foreground ,gH-256-black :weight bold))))

   `(magit-log-head-label-remote
     ((,gH-class (:background ,gH-green :foreground ,gH-black :weight bold))
      (,gH-256-class (:background ,gH-256-green :foreground ,gH-256-black :weight bold))))

   `(magit-log-head-label-tags
     ((,gH-class (:background ,gH-magenta :foreground ,gH-black :weight bold))
      (,gH-256-class (:background ,gH-256-magenta :foreground ,gH-256-black :weight bold))))

   `(magit-log-head-label-wip
     ((,gH-class (:background ,gH-cyan :foreground ,gH-black :weight bold))
      (,gH-256-class (:background ,gH-256-cyan :foreground ,gH-256-black :weight bold))))

   `(magit-log-sha1
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(magit-process-ng
     ((,gH-class (:foreground ,gH-bright-orange :weight bold))
      (,gH-256-class (:foreground ,gH-256-bright-orange :weight bold))))

   `(magit-process-ok
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

   `(magit-section-heading
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(magit-section-highlight
     ((,gH-class (:weight bold))
      (,gH-256-class (:weight bold))))

   `(section-heading-selection
     ((,gH-class (:foreground ,gH-red :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :weight bold))))

   `(magit-section-title
     ((,gH-class (:background ,gH-black :foreground ,gH-red :weight bold))
      (,gH-256-class (:background ,(if gH-transparent-background nil gH-256-black) :foreground ,gH-256-red :weight bold))))

   `(magit-cherry-equivalent
     ((,gH-class (:foreground ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-magenta))))

   `(magit-cherry-unmatched
     ((,gH-class (:foreground ,gH-cyan))
      (,gH-256-class (:foreground ,gH-256-cyan))))

   `(magit-reflog-checkout
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(magit-reflog-cherry-pick
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(magit-bisect-bad
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(magit-bisect-good
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(magit-bisect-skip
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(magit-diff-conflict-heading
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(magit-dimmed
     ((,gH-class (:foreground ,gH-white))
      (,gH-256-class (:foreground ,gH-256-white))))

   `(magithub-ci-no-status
     ((,gH-class (:foreground ,gH-gray5))
      (,gH-256-class (:foreground ,gH-256-gray5))))

   `(magithub-issue-number
     ((,gH-class (:foreground ,gH-white))
      (,gH-256-class (:foreground ,gH-256-white))))

   `(magithub-notification-reason
     ((,gH-class (:foreground ,gH-white))
      (,gH-256-class (:foreground ,gH-256-white))))

;;;;; smerge
   `(smerge-base
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(smerge-markers
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(smerge-mine
     ((,gH-class (:foreground nil))
      (,gH-256-class (:foreground ,nil))))

   `(smerge-other
     ((,gH-class (:background ,gH-bright-black))
      (,gH-256-class (:background ,gH-256-bright-black))))

   `(smerge-refined-added
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(smerge-refined-changed
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(smerge-refined-removed
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(smerge-upper
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(smerge-lower
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

;;;;; man
   `(Man-overstrike
     ((,gH-class (:foreground ,gH-blue :weight bold))
      (,gH-256-class (:foreground ,gH-256-blue :weight bold))))

   `(Man-reverse
     ((,gH-class (:foreground ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-magenta))))

   `(Man-underline
     ((,gH-class (:foreground ,gH-green :underline t))
      (,gH-256-class (:foreground ,gH-256-green :underline t))))


;;;;; markdown
   `(markdown-header-face-1
     ((,gH-class (:weight bold :foreground ,gH-blue :height ,(if gH-org-height 1.3 1.0)))
      (,gH-256-class (:weight bold :foreground ,gH-256-blue :height ,(if gH-org-height 1.3 1.0)))))

   `(markdown-header-face-2
     ((,gH-class (:weight bold :foreground ,gH-bright-cyan :height ,(if gH-org-height 1.2 1.0)))
      (,gH-256-class (:weight bold :foreground ,gH-256-bright-cyan :height ,(if gH-org-height 1.2 1.0)))))

   `(markdown-header-face-3
     ((,gH-class (:bold nil :foreground ,gH-bright-green :height ,(if gH-org-height 1.1 1.0)))
      (,gH-256-class (:bold nil :foreground ,gH-256-bright-green :height ,(if gH-org-height 1.1 1.0)))))

   `(markdown-header-face-4
     ((,gH-class (:bold nil :foreground ,gH-yellow))
      (,gH-256-class (:bold nil :foreground ,gH-256-yellow))))

   `(markdown-header-face-5
     ((,gH-class (:bold nil :foreground ,gH-blue))
      (,gH-256-class (:bold nil :foreground ,gH-256-blue))))

   `(markdown-header-face-6
     ((,gH-class (:bold nil :foreground ,gH-cyan))
      (,gH-256-class (:bold nil :foreground ,gH-256-cyan))))

   `(markdown-html-tag-delimiter-face
     ((,gH-class (:bold nil :foreground ,gH-gray5))
      (,gH-256-class (:bold nil :foreground ,gH-256-gray5))))

   `(markdown-list-face
     ((,gH-class (:bold nil :foreground ,gH-gray5))
      (,gH-256-class (:bold nil :foreground ,gH-256-gray5))))

   `(markdown-markup-face
     ((,gH-class (:bold nil :foreground ,gH-gray5))
      (,gH-256-class (:bold nil :foreground ,gH-256-gray5))))

;;;;; mu4e
   `(mu4e-cited-1-face
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(mu4e-cited-7-face
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(mu4e-header-marks-face
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(mu4e-header-key-face
     ((,gH-class (:foreground ,gH-cyan :weight bold))
      (,gH-256-class (:foreground ,gH-256-cyan :weight bold))))

   `(mu4e-view-url-number-face
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(mu4e-unread-face
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

;;;;; neotree
   `(neo-dir-link-face
     ((,gH-class (:foreground ,gH-red :weight bold))
      (,gH-256-class (:foreground ,gH-256-red :weight bold))))

   `(neo-expand-btn-face
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(neo-file-link-face
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(neo-root-dir-face
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

;;;;; org
   `(org-agenda-clocking
     ((,gH-class (:background ,gH-magenta :foreground ,gH-green))
      (,gH-256-class (:background ,gH-256-magenta :foreground ,gH-256-green))))

   `(org-agenda-date
     ((,gH-class (:foreground ,gH-blue :height ,(if gH-org-height 1.1 1.0)))
      (,gH-256-class (:foreground ,gH-256-blue :height ,(if gH-org-height 1.1 1.0)))))

   `(org-agenda-date-today
     ((,gH-class (:foreground ,gH-red :slant italic :weight bold :height ,(if gH-org-height 1.3 1.0)))
      (,gH-256-class (:foreground ,gH-256-red :slant italic :weight bold :height ,(if gH-org-height 1.3 1.0)))))

   `(org-agenda-date-weekend
     ((,gH-class (:weight bold :foreground ,gH-blue))
      (,gH-256-class (:weight bold :foreground ,gH-256-blue))))

   `(org-agenda-done
     ((,gH-class (:foreground ,gH-green :height ,(if gH-org-height 1.2 1.0)))
      (,gH-256-class (:foreground ,gH-256-green :height ,(if gH-org-height 1.2 1.0)))))

   `(org-agenda-structure
     ((,gH-class (:weight bold :foreground ,gH-green))
      (,gH-256-class (:weight bold :foreground ,gH-256-green))))

   `(org-block
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(org-block-begin-line
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(org-block-end-line
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(org-clock-overlay
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(org-code
     ((,gH-class (:foreground ,gH-cyan))
      (,gH-256-class (:foreground ,gH-256-cyan))))

   `(org-column
     ((,gH-class (:background ,gH-magenta))
      (,gH-256-class (:background ,gH-256-magenta))))

   `(org-column-title
     ((,gH-class (:background ,gH-magenta))
      (,gH-256-class (:background ,gH-256-magenta))))

   `(org-date
     ((,gH-class (:underline t :foreground ,gH-blue))
      (,gH-256-class (:underline t :foreground ,gH-256-blue))))

   `(org-date-selected
     ((,gH-class (:background ,gH-yellow :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-yellow :foreground ,gH-256-black))))

   `(org-document-info-keyword
     ((,gH-class (:foreground ,gH-white))
      (,gH-256-class (:foreground ,gH-256-white))))

   `(org-document-info
     ((,gH-class (:foreground ,gH-bright-magenta))
      (,gH-256-class (:foreground ,gH-256-bright-magenta))))

   `(org-document-title
     ((,gH-class (:foreground ,gH-yellow :weight bold :height ,(if gH-org-height 1.4 1.0)))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold :height ,(if gH-org-height 1.4 1.0)))))

   `(org-done
     ((,gH-class (:foreground ,gH-green :weight bold))
      (,gH-256-class (:foreground ,gH-256-green :weight bold))))

   `(org-ellipsis
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(org-footnote
     ((,gH-class (:underline t :foreground ,gH-foreground))
      (,gH-256-class (:underline t :foreground ,gH-256-bright-white))))

   `(org-hide
     ((,gH-class (:foreground ,gH-darker-gray :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-darker-gray :background ,(if gH-transparent-background nil gH-256-black)))))

   `(org-kbd
     ((,gH-class (:inherit region :foreground ,gH-foreground :box (:line-width 1 :style released-button)))
      (,gH-256-class (:inherit region :foreground ,gH-256-bright-white :box (:line-width 1 :style released-button)))))

   `(org-level-1
     ((,gH-class (:weight bold :foreground ,gH-bright-blue :height ,(if gH-org-height 1.3 1.0)))
      (,gH-256-class (:weight bold :foreground ,gH-256-bright-blue :height ,(if gH-org-height 1.3 1.0)))))

   `(org-level-2
     ((,gH-class (:weight bold :foreground ,gH-green :height ,(if gH-org-height 1.2 1.0)))
      (,gH-256-class (:weight bold :foreground ,gH-256-green :height ,(if gH-org-height 1.2 1.0)))))

   `(org-level-3
     ((,gH-class (:bold nil :foreground ,gH-yellow :height ,(if gH-org-height 1.1 1.0)))
      (,gH-256-class (:bold nil :foreground ,gH-256-yellow :height ,(if gH-org-height 1.1 1.0)))))

   `(org-level-4
     ((,gH-class (:bold nil :foreground ,gH-blue))
      (,gH-256-class (:bold nil :foreground ,gH-256-blue))))

   `(org-level-5
     ((,gH-class (:bold nil :foreground ,gH-cyan))
      (,gH-256-class (:bold nil :foreground ,gH-256-cyan))))

   `(org-level-6
     ((,gH-class (:bold nil :foreground ,gH-green))
      (,gH-256-class (:bold nil :foreground ,gH-256-green))))

   `(org-level-7
     ((,gH-class (:bold nil :foreground ,gH-bright-orange))
      (,gH-256-class (:bold nil :foreground ,gH-256-orange))))

   `(org-level-8
     ((,gH-class (:bold nil :foreground ,gH-bright-magenta))
      (,gH-256-class (:bold nil :foreground ,gH-256-bright-magenta))))

   `(org-link
     ((,gH-class (:foreground ,gH-white :underline t))
      (,gH-256-class (:foreground ,gH-256-white :underline t))))

   `(org-meta-line
     ((,gH-class (:foreground ,gH-white))
      (,gH-256-class (:foreground ,gH-256-white))))

   `(org-mode-line-clock-overrun
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(org-mode-line-clock
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(org-priority
     ((,gH-class (:foreground ,gH-bright-orange :weight bold))
      (,gH-256-class (:foreground ,gH-256-bright-orange :weight bold))))

   `(org-quote
     ((,gH-class (:inherit org-block :slant italic))
      (,gH-256-class (:inherit org-block :slant italic))))

   `(org-scheduled
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(org-scheduled-today
     ((,gH-class (:foreground ,gH-yellow :height ,(if gH-org-height 1.2 1.0)))
      (,gH-256-class (:foreground ,gH-256-yellow :height ,(if gH-org-height 1.2 1.0)))))

   `(org-sexp-date
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(org-special-keyword
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(org-table
     ((,gH-class (:foreground ,gH-foreground :background ,gH-gray1))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,gH-256-gray1))))

   `(org-time-grid
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(org-todo
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

   `(org-verbatim
     ((,gH-class (:foreground ,gH-bright-orange))
      (,gH-256-class (:foreground ,gH-256-bright-orange))))

   `(org-verse
     ((,gH-class (:inherit org-block :slant italic))
      (,gH-256-class (:inherit org-block :slant italic))))

   `(org-warning
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

;;;;; (TODO) Outline / Outshine
   ;; outline-1
   ;; outline-2
   ;; outline-3
   ;; oi-match-face
   ;; oi-face-1
   ;; oi-face-2
   ;; oi-face-3
;;;;; perspective
   `(persp-selected-face
     ((,gH-class (:weight bold :foreground ,gH-yellow))
      (,gH-256-class (:weight bold :foreground ,gH-256-yellow))))

;;;;; popup
   `(popup-face
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-white))))

   `(popup-tip-face
     ((,gH-class (:background ,gH-bright-blue :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-blue :foreground ,gH-256-black))))

   `(popup-menu-face
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-white))))

   `(popup-menu-selection-face
     ((,gH-class (:background ,gH-bright-blue :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-blue :foreground ,gH-256-black))))

   `(popup-menu-mouse-face
     ((,gH-class (:inherit highlight))
      (,gH-256-class (:inherit highlight))))

   `(popup-isearch-match
     ((,gH-class (:inherit match))
      (,gH-256-class (:inherit match))))

   `(popup-scroll-bar-foreground-face
     ((,gH-class (:background ,gH-bright-black))
      (,gH-256-class (:background ,gH-256-bright-black))))

   `(popup-scroll-bar-background-face
     ((,gH-class (:background ,gH-bright-black))
      (,gH-256-class (:background ,gH-256-bright-black))))


;;;;; mode-line
   `(powerline-active1
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-white))))

   `(powerline-active2
     ((,gH-class (:foreground ,gH-foreground :background ,gH-darker-gray))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,gH-256-darker-gray))))

   `(powerline-inactive1
     ((,gH-class (:background ,gH-darker-gray :foreground ,gH-white))
      (,gH-256-class (:background ,gH-256-darker-gray :foreground ,gH-256-white))))

   `(powerline-inactive2
     ((,gH-class (:background ,gH-darker-gray :foreground ,gH-white))
      (,gH-256-class (:background ,gH-256-darker-gray :foreground ,gH-256-white))))

   `(mode-line
     ((,gH-class (:foreground ,gH-white :background ,gH-darker-gray))
      (,gH-256-class (:foreground ,gH-256-white :background ,gH-256-darker-gray))))

   `(mode-line-inactive
     ((,gH-class (:foreground ,gH-white :background ,gH-darker-gray))
      (,gH-256-class (:foreground ,gH-256-white :background ,gH-256-darker-gray))))

   `(mode-line-buffer-id
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(mode-line-highlight
     ((,gH-class (:background ,gH-darker-gray :box (:color ,gH-magenta :line-width 1)))
      (,gH-256-class (:background ,gH-256-darker-gray :box (:color ,gH-256-magenta :line-width 1)))))

   `(mode-line-buffer-id-inactive
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(magit-mode-line-process
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(mode-line-emphasis
     ((,gH-class (:weight bold :foreground ,gH-yellow))
      (,gH-256-class (:weight bold :foreground ,gH-256-yellow))))

   `(spaceline-python-venv
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(spaceline-flycheck-error
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(spaceline-flycheck-info
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(spaceline-flycheck-warning
     ((,gH-class (:foreground ,gH-bright-orange))
      (,gH-256-class (:foreground ,gH-256-bright-orange))))

   `(spaceline-evil-normal
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-white))))

   `(spaceline-evil-insert
     ((,gH-class (:background ,gH-foreground :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-white :foreground ,gH-256-black))))

   `(spaceline-evil-replace
     ((,gH-class (:background ,gH-bright-red :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-bright-red :foreground ,gH-256-bright-white))))

   `(spaceline-evil-visual
     ((,gH-class (:background ,gH-cyan :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-cyan :foreground ,gH-256-black))))

   `(spaceline-evil-motion
     ((,gH-class (:background ,gH-bright-magenta :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-magenta :foreground ,gH-256-black))))

   `(spaceline-evil-emacs
     ((,gH-class (:background ,gH-orange :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-orange :foreground ,gH-256-bright-white))))

   `(spaceline-unmodified
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(spaceline-modified
     ((,gH-class (:background ,gH-bright-orange :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-orange :foreground ,gH-256-black))))

   `(spaceline-read-only
     ((,gH-class (:background ,gH-gray1 :foreground ,gH-orange))
      (,gH-256-class (:background ,gH-256-gray1 :foreground ,gH-256-orange))))

   `(spaceline-highlight-face
     ((,gH-class (:background ,gH-yellow :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-yellow :foreground ,gH-256-black))))


;;;;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face
     ((,gH-class :foreground ,gH-foreground)
      (,gH-256-class :foreground ,gH-256-bright-white)))

   `(rainbow-delimiters-depth-2-face
     ((,gH-class :foreground ,gH-bright-blue)
      (,gH-256-class :foreground ,gH-256-bright-blue)))

   `(rainbow-delimiters-depth-3-face
     ((,gH-class :foreground ,gH-foreground)
      (,gH-256-class :foreground ,gH-256-bright-white)))

   `(rainbow-delimiters-depth-4-face
     ((,gH-class :foreground ,gH-bright-cyan)
      (,gH-256-class :foreground ,gH-256-bright-cyan)))

   `(rainbow-delimiters-depth-5-face
     ((,gH-class :foreground ,gH-bright-green)
      (,gH-256-class :foreground ,gH-256-bright-green)))

   `(rainbow-delimiters-depth-6-face
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(rainbow-delimiters-depth-7-face
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

   `(rainbow-delimiters-depth-8-face
     ((,gH-class :foreground ,gH-yellow)
      (,gH-256-class :foreground ,gH-256-yellow)))

   `(rainbow-delimiters-unmatched-face
     ((,gH-class :foreground ,gH-red)
      (,gH-256-class :foreground ,gH-256-red)))

   `(rainbow-delimiters-mismatched-face
     ((,gH-class :foreground ,gH-bright-red)
      (,gH-256-class :foreground ,gH-256-bright-red)))

   ;; `(rainbow-delimiters-unmatched-face
   ;;   ((,gH-class :foreground ,gH-red :overline t :inhert bold)
   ;;    (,gH-256-class :foreground ,gH-256-red :overline t :inhert bold)))

   ;; `(rainbow-delimiters-mismatched-face
   ;;   ((,gH-class :foreground ,gH-red :overline t :weight bold)
   ;;    (,gH-256-class :foreground ,gH-256-red :overline t :weight bold)))


;;;;; sh
   `(sh-heredoc
     ((,gH-class (:foreground ,gH-green :weight bold))
      (,gH-256-class (:foreground ,gH-256-green :weight bold))))

   `(sh-quoted-exec
     ((,gH-class (:foreground ,gH-orange))
      (,gH-256-class (:foreground ,gH-256-orange))))

;;;;; shm
   `(shm-current-face
     ((,gH-class (:background ,gH-green :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-green :foreground ,gH-256-black))))

   `(shm-quarantine-face
     ((,gH-class (:background ,gH-gray1))
      (,gH-256-class (:background ,gH-256-gray1))))


;;;;; show-paren
   `(show-paren-match
     ((,gH-class (:foreground ,gH-magenta :weight bold))
      (,gH-256-class (:background ,gH-256-magenta :weight bold))))

   `(show-paren-mismatch
     ((,gH-class (:background ,gH-red :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-red :foreground ,gH-256-bright-white))))


;;;;; paren-face
   `(parenthesis
     ((,gH-class (:foreground ,gH-gray5))
      (,gH-256-class (:foreground ,gH-256-gray5))))


;;;;; smartparens
   `(sp-pair-overlay-face
     ((,gH-class (:background ,gH-magenta :foreground nil))
      (,gH-256-class (:background ,gH-256-magenta :foreground nil))))

   `(sp-show-pair-match-face
     ((,gH-class (:foreground ,gH-magenta :weight bold))
      (,gH-256-class (:foreground ,gH-256-magenta :weight bold))))

   `(sp-wrap-overlay-closing-pair
     ((,gH-class (:background ,gH-magenta :foreground, gH-bright-yellow))
      (,gH-256-class (:background ,gH-256-magenta :foreground, gH-256-bright-yellow))))


;;;;; evil-snipe
   `(evil-snipe-first-match-face
     ((,gH-class (:foreground ,gH-magenta :weight bold))
      (,gH-256-class (:foreground ,gH-256-magenta :weight bold))))

   `(evil-snipe-matches-face
     ((,gH-class (:foreground ,gH-magenta :weight bold))
      (,gH-256-class (:foreground ,gH-256-magenta :weight bold))))


;;;;; spacemacs
   `(spacemacs-normal-face
     ((,gH-class (:background ,gH-bright-black :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-bright-black :foreground ,gH-256-bright-white))))

   `(spacemacs-insert-face
     ((,gH-class (:background ,gH-foreground :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-white :foreground ,gH-256-black))))

   `(spacemacs-replace-face
     ((,gH-class (:background ,gH-bright-red :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-bright-red :foreground ,gH-256-bright-white))))

   `(spacemacs-visual-face
     ((,gH-class (:background ,gH-bright-cyan :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-cyan :foreground ,gH-256-black))))

   `(spacemacs-motion-face
     ((,gH-class (:background ,gH-magenta :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-magenta :foreground ,gH-256-bright-white))))

   `(spacemacs-emacs-face
     ((,gH-class (:background ,gH-orange :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-orange :foreground ,gH-256-bright-white))))

   `(spacemacs-hybrid-face
     ((,gH-class (:background ,gH-bright-orange :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-orange :foreground ,gH-256-black))))

   `(spacemacs-lisp-face
     ((,gH-class (:background ,gH-green :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-green :foreground ,gH-256-black))))

   `(spacemacs-evilified-face
     ((,gH-class (:background ,gH-bright-yellow :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-yellow :foreground ,gH-256-black))))

   `(spacemacs-helm-navigation-ms-face
     ((,gH-class (:foreground ,gH-foreground :slant italic))
      (,gH-256-class (:foreground ,gH-256-bright-white :slant italic))))

   `(spacemacs-helm-navigation-ts-face
     ((,gH-class (:foreground ,gH-foreground :slant italic))
      (,gH-256-class (:foreground ,gH-256-bright-white :slant italic))))

   `(spacemacs-transient-state-title-face
     ((,gH-class (:background nil :foreground ,gH-green :box nil :weight bold))
      (,gH-256-class (:background nil :foreground ,gH-256-green :box nil :weight bold))))

   `(spacemacs-ido-navigation-ts-face
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(spacemacs-iedit-face
     ((,gH-class (:background ,gH-blue :foreground ,gH-foreground))
      (,gH-256-class (:background ,gH-256-blue :foreground ,gH-256-bright-white))))

   `(spacemacs-iedit-insert-face
     ((,gH-class (:background ,gH-bright-blue :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-bright-blue :foreground ,gH-256-black))))

   `(spacemacs-micro-state-binding-face
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

;;;;; swiper
   `(swiper-line-face
     ((,gH-class (:background ,gH-bright-black :weight bold))
      (,gH-256-class (:background ,gH-bright-black :weight bold))))

   `(swiper-match-face-1
     ((,gH-class (:weight bold))
      (,gH-256-class (:weight bold))))

   `(swiper-match-face-2
     ((,gH-class (:foreground ,gH-magenta :underline t))
      (,gH-256-class (:foreground ,gH-256-magenta :underline t))))

   `(swiper-match-face-3
     ((,gH-class (:foreground ,gH-yellow :underline t))
      (,gH-256-class (:foreground ,gH-256-yellow :underline t))))

   `(swiper-match-face-4
     ((,gH-class (:foreground ,gH-bright-green :underline t))
      (,gH-256-class (:foreground ,gH-256-bright-green :underline t))))


;;;;; term
   `(term
     ((,gH-class (:foreground ,gH-foreground :background ,gH-black))
      (,gH-256-class (:foreground ,gH-256-bright-white :background ,(if gH-transparent-background nil gH-256-black)))))

   `(term-color-black
     ((,gH-class (:foreground ,gH-black))
      (,gH-256-class (:foreground ,gH-256-black))))

   `(term-color-blue
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(term-color-cyan
     ((,gH-class (:foreground ,gH-cyan))
      (,gH-256-class (:foreground ,gH-256-cyan))))

   `(term-color-green
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(term-color-magenta
     ((,gH-class (:foreground ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-magenta))))

   `(term-color-red
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(term-color-white
     ((,gH-class (:foreground ,gH-white))
      (,gH-256-class (:foreground ,gH-256-white))))

   `(term-color-yellow
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))


;;;;; web-mode
   `(web-mode-builtin-face
     ((,gH-class (:inherit ,font-lock-builtin-face))
      (,gH-256-class (:inherit ,font-lock-builtin-face))))

   `(web-mode-comment-face
     ((,gH-class (:inherit ,font-lock-comment-face))
      (,gH-256-class (:inherit ,font-lock-comment-face))))

   `(web-mode-constant-face
     ((,gH-class (:inherit ,font-lock-constant-face))
      (,gH-256-class (:inherit ,font-lock-constant-face))))

   `(web-mode-doctype-face
     ((,gH-class (:inherit ,font-lock-comment-face))
      (,gH-256-class (:inherit ,font-lock-comment-face))))

   `(web-mode-function-name-face
     ((,gH-class (:inherit ,font-lock-function-name-face))
      (,gH-256-class (:inherit ,font-lock-function-name-face))))

   `(web-mode-html-attr-name-face
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

   `(web-mode-html-attr-value-face
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(web-mode-html-tag-face
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(web-mode-html-tag-bracket-face
     ((,gH-class (:foreground ,gH-gray5))
      (,gH-256-class (:foreground ,gH-256-gray5))))

   `(web-mode-keyword-face
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(web-mode-string-face
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(web-mode-symbol-face
     ((,gH-class (:foreground ,gH-bright-blue))
      (,gH-256-class (:foreground ,gH-256-bright-blue))))

   `(web-mode-type-face
     ((,gH-class (:inherit ,font-lock-type-face))
      (,gH-256-class (:inherit ,font-lock-type-face))))

   `(web-mode-warning-face
     ((,gH-class (:inherit ,font-lock-warning-face))
      (,gH-256-class (:inherit ,font-lock-warning-face))))

;;;;; CSS
   `(css-selector
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(css-property
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

;;;;; XML
   `(nxml-element-local-name
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(nxml-attribute-local-name
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))

;;;;; which-key
   `(which-key-command-description-face
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(which-key-group-description-face
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(which-key-key-face
     ((,gH-class (:foreground ,gH-yellow :weight bold))
      (,gH-256-class (:foreground ,gH-256-yellow :weight bold))))

   `(which-key-separator-face
     ((,gH-class (:background nil :foreground ,gH-bright-green))
      (,gH-256-class (:background nil :foreground ,gH-256-bright-green))))

   `(which-key-special-key-face
     ((,gH-class (:background ,gH-yellow :foreground ,gH-black))
      (,gH-256-class (:background ,gH-256-yellow :foreground ,gH-256-black))))


;;;;; which-function-mode
   `(which-func
     ((,gH-class (:foreground ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-yellow))))


;;;;; whitespace-mode
   `(whitespace-empty
     ((,gH-class (:background nil :foreground ,gH-yellow))
      (,gH-256-class (:background nil :foreground ,gH-256-yellow))))

   `(whitespace-indentation
     ((,gH-class (:background nil :foreground ,gH-bright-orange))
      (,gH-256-class (:background nil :foreground ,gH-256-bright-orange))))

   `(whitespace-line
     ((,gH-class (:background nil :foreground ,gH-green))
      (,gH-256-class (:background nil :foreground ,gH-256-green))))

   `(whitespace-newline
     ((,gH-class (:background nil :foreground ,gH-green))
      (,gH-256-class (:background nil :foreground ,gH-256-green))))

   `(whitespace-space
     ((,gH-class (:background nil :foreground ,gH-bright-black))
      (,gH-256-class (:background nil :foreground ,gH-256-bright-black))))

   `(whitespace-space-after-tab
     ((,gH-class (:background nil :foreground ,gH-yellow))
      (,gH-256-class (:background nil :foreground ,gH-256-yellow))))

   `(whitespace-space-before-tab
     ((,gH-class (:background nil :foreground ,gH-yellow))
      (,gH-256-class (:background nil :foreground ,gH-256-yellow))))

   `(whitespace-tab
     ((,gH-class (:background nil))
      (,gH-256-class (:background nil))))

   `(whitespace-trailing
     ((,gH-class (:background ,gH-red :foreground ,gH-bright-orange))
      (,gH-256-class (:background ,gH-256-red :foreground ,gH-256-bright-orange))))


;;;;; ctbl
   `(ctbl:face-cell-select
     ((,gH-class (:foreground ,gH-black :background ,gH-bright-magenta))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-bright-magenta))))

   `(ctbl:face-continue-bar
     ((,gH-class (:foreground ,gH-black :background ,gH-bright-yellow))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-bright-yellow))))

   `(ctbl:face-row-select
     ((,gH-class (:foreground ,gH-black :background ,gH-bright-blue))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-bright-blue))))

;;;;; hlt
   `(hlt-property-highlight
     ((,gH-class (:foreground ,gH-black :background ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-yellow))))

   `(hlt-regexp-level-1
     ((,gH-class (:foreground ,gH-black :background ,gH-bright-magenta))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-bright-magenta))))

   `(hlt-regexp-level-2
     ((,gH-class (:foreground ,gH-black :background ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-bright-green))))

   `(hlt-regexp-level-3
     ((,gH-class (:foreground ,gH-black :background ,gH-magenta))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-magenta))))

   `(hlt-regexp-level-4
     ((,gH-class (:foreground ,gH-black :background ,gH-bright-yellow))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-bright-yellow))))

   `(hlt-regexp-level-5
     ((,gH-class (:foreground ,gH-black :background ,gH-green))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-green))))

   `(hlt-regexp-level-6
     ((,gH-class (:foreground ,gH-black :background ,gH-bright-blue))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-bright-blue))))

   `(hlt-regexp-level-7
     ((,gH-class (:foreground ,gH-black :background ,gH-cyan))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-cyan))))

   `(hlt-regexp-level-8
     ((,gH-class (:foreground ,gH-black :background ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-blue))))

;;;;; reb
   `(reb-match-0
     ((,gH-class (:foreground ,gH-black :background ,gH-bright-blue))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-bright-blue))))

   `(reb-match-1
     ((,gH-class (:foreground ,gH-black :background ,gH-bright-cyan))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-bright-cyan))))

   `(reb-match-2
     ((,gH-class (:foreground ,gH-black :background ,gH-green))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-green))))

   `(reb-match-3
     ((,gH-class (:foreground ,gH-black :background ,gH-yellow))
      (,gH-256-class (:foreground ,gH-256-black :background ,gH-256-yellow))))

;;;;; other, need more work
   `(ac-completion-face
     ((,gH-class (:underline t :foreground ,gH-red))
      (,gH-256-class (:underline t :foreground ,gH-256-red))))

   `(epc:face-title
     ((,gH-class :foreground ,gH-blue :weight bold)
      (,gH-256-class :foreground ,gH-256-blue :weight bold)))

   `(ffap
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(flx-highlight-face
     ((,gH-class (:foreground ,gH-green :underline nil))
      (,gH-256-class (:foreground ,gH-256-green :underline nil))))

   `(icompletep-determined
     ((,gH-class :foreground ,gH-red)
      (,gH-256-class :foreground ,gH-256-red)))

   `(js2-external-variable
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(js2-function-param
     ((,gH-class (:foreground ,gH-bright-magenta))
      (,gH-256-class (:foreground ,gH-256-bright-magenta))))

   `(js2-jsdoc-html-tag-delimiter
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(js2-jsdoc-html-tag-name
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(js2-jsdoc-value
     ((,gH-class (:foreground ,gH-bright-green))
      (,gH-256-class (:foreground ,gH-256-bright-green))))

   `(js2-private-function-call
     ((,gH-class (:foreground ,gH-bright-magenta))
      (,gH-256-class (:foreground ,gH-256-bright-magenta))))

   `(js2-private-member
     ((,gH-class (:foreground ,gH-foreground))
      (,gH-256-class (:foreground ,gH-256-bright-white))))

   `(js2-object-property
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(js3-error-face
     ((,gH-class (:underline ,gH-bright-orange))
      (,gH-256-class (:underline ,gH-256-bright-orange))))

   `(js3-external-variable-face
     ((,gH-class (:foreground ,gH-blue))
      (,gH-256-class (:foreground ,gH-256-blue))))

   `(js3-function-param-face
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(js3-instance-member-face
     ((,gH-class (:foreground ,gH-bright-magenta))
      (,gH-256-class (:foreground ,gH-256-bright-magenta))))

   `(js3-jsdoc-tag-face
     ((,gH-class (:foreground ,gH-red))
      (,gH-256-class (:foreground ,gH-256-red))))

   `(js3-warning-face
     ((,gH-class (:underline ,gH-red))
      (,gH-256-class (:underline ,gH-256-red))))

   `(slime-repl-inputed-output-face
     ((,gH-class (:foreground ,gH-green))
      (,gH-256-class (:foreground ,gH-256-green))))

   `(trailing-whitespace
     ((,gH-class :foreground nil :background ,gH-red)
      (,gH-256-class :foreground nil :background ,gH-256-red)))

   `(undo-tree-visualizer-current-face
     ((,gH-class :foreground ,gH-red)
      (,gH-256-class :foreground ,gH-256-red)))

   `(undo-tree-visualizer-default-face
     ((,gH-class :foreground ,gH-foreground)
      (,gH-256-class :foreground ,gH-256-bright-white)))

   `(undo-tree-visualizer-register-face
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

   `(undo-tree-visualizer-unmodified-face
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(undo-tree-visualizer-active-branch-face
     ((,gH-class :foreground ,gH-bright-magenta)
      (,gH-256-class :foreground ,gH-256-bright-magenta)))

   `(persp-face-lighter-buffer-not-in-persp
     ((,gH-class :background ,gH-red :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-red :foreground ,gH-256-bright-white)))

   `(pulse-highlight-face
     ((,gH-class :background ,gH-green :foreground ,gH-black)
      (,gH-256-class :background ,gH-256-green :foreground ,gH-256-black)))

   `(pulse-highlight-start-face
     ((,gH-class :background ,gH-bright-green :foreground ,gH-black)
      (,gH-256-class :background ,gH-256-bright-green :foreground ,gH-256-black)))

   `(custom-invalid
     ((,gH-class :background ,gH-bright-red :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-bright-red :foreground ,gH-256-bright-white)))

   `(holiday
     ((,gH-class :background ,gH-bright-magenta :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-bright-magenta :foreground ,gH-256-bright-white)))

   `(whitespace-trailing
     ((,gH-class :background ,gH-red :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-red :foreground ,gH-256-bright-white)))

   `(whitespace-big-indent
     ((,gH-class :background ,gH-bright-red :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-bright-red :foreground ,gH-256-bright-white)))

   `(whitespace-hspace
     ((,gH-class :background ,gH-bright-blue :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-bright-blue :foreground ,gH-256-bright-white)))

;;;;; Slack
   `(lui-button-face
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(lui-highlight-face
     ((,gH-class :foreground ,gH-magenta)
      (,gH-256-class :foreground ,gH-256-magenta)))

   `(lui-time-stamp-face
     ((,gH-class :foreground ,gH-white)
      (,gH-256-class :foreground ,gH-256-white)))

   `(slack-profile-image-face
     ((,gH-class :background ,gH-foreground :foreground ,gH-black)
      (,gH-256-class :background ,gH-256-bright-white :foreground ,gH-256-black)))

   `(slack-preview-face
     ((,gH-class :foreground ,gH-cyan)
      (,gH-256-class :foreground ,gH-256-cyan)))

   `(slack-message-output-header
     ((,gH-class :foreground ,gH-yellow :weight bold)
      (,gH-256-class :foreground ,gH-256-yellow :weight bold)))

;;;;; Message
   `(message-header-cc
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(message-header-newsgroups
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(message-header-subject
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(message-header-to
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

;;;;; Alert
   `(alert-low-face
     ((,gH-class :foreground ,gH-blue :weight bold)
      (,gH-256-class :foreground ,gH-256-blue :weight bold)))

   `(alert-moderate-face
     ((,gH-class :foreground ,gH-yellow :weight bold)
      (,gH-256-class :foreground ,gH-256-yellow :weight bold)))

;;;;; Custom
   `(custom-comment-tag
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(custom-face-tag
     ((,gH-class :foreground ,gH-blue :weight bold)
      (,gH-256-class :foreground ,gH-256-blue :weight bold)))

   `(custom-group-tag
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(custom-state
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

   `(custom-set
     ((,gH-class :background ,gH-bright-blue :foreground ,gH-black)
      (,gH-256-class :background ,gH-256-bright-blue :foreground ,gH-256-black)))

   `(custom-modified
     ((,gH-class :background ,gH-blue :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-blue :foreground ,gH-256-bright-white)))

   `(custom-themed
     ((,gH-class :background ,gH-blue :foreground ,gH-black)
      (,gH-256-class :background ,gH-256-blue :foreground ,gH-256-black)))

   `(custom-variable-tag
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(custom-changed
     ((,gH-class :background ,gH-blue :foreground ,gH-black)
      (,gH-256-class :background ,gH-256-blue :foreground ,gH-256-black)))

   `(custom-comment
     ((,gH-class :background ,gH-white :foreground ,gH-black)
      (,gH-256-class :background ,gH-256-white :foreground ,gH-256-black)))

;;;;; LSP
   `(lsp-ui-sideline-global
     ((,gH-class :foreground ,gH-gray5)
      (,gH-256-class :foreground ,gH-256-gray5)))

;;;;; widget
   `(widget-field
     ((,gH-class :background ,gH-gray2 :foreground ,gH-foreground)
      (,gH-256-class :background ,gH-256-gray2 :foreground ,gH-256-bright-white)))

   `(widget-documentation
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

;;;;; Misc
   `(epa-string
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(imenu-list-entry-face-0
     ((,gH-class :foreground ,gH-magenta)
      (,gH-256-class :foreground ,gH-256-magenta)))

   `(imenu-list-entry-face-1
     ((,gH-class :foreground ,gH-green)
      (,gH-256-class :foreground ,gH-256-green)))

   `(imenu-list-entry-face-2
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

   `(imenu-list-entry-face-3
     ((,gH-class :foreground ,gH-blue)
      (,gH-256-class :foreground ,gH-256-blue)))

;;;;; mmm
   `(mmm-default-submode-face
     ((,gH-class :background ,(if gH-transparent-background nil gH-256-black))
      (,gH-256-class :background nil)))
   )

;;;;; Ansi colors for terminal
  (custom-theme-set-variables
   'gorgeHousse
   `(ansi-color-names-vector [,gH-black ,gH-red ,gH-green ,gH-yellow ,gH-blue ,gH-magenta ,gH-cyan ,gH-white])
   ))

;;;###autoload
(when load-file-name
  (add-to-list 'custom-theme-load-path
	       (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'gorgeHousse)

;; Local Variables:
;; no-byte-compile: t
;; End:

;;; gorgeHousse-theme.el ends here
